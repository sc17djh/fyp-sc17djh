# FYP sc17djh

A repository to implement the major design components of COMP3931 Final Year Project Design 1

### INTRO ###

This repository maintains the prototype code used to implement and evaluate design 1 specified in the FYP report. 

### SETUP ###
The following requirements must be installed:
1. MySQL 8.0.19 server for the machine running the 'Server' server
2. SQLite for the machine running the 'KeyServer' server
3. the python modules in the requirements.txt list. This can be done using 'pip3 install -r requirements.txt'

The MySQL server must be configured with a database called 'patient_data'. The server will handle creating the required tables. The credentials used to access the server must be username 'root' and password 'password'.

The generate_data.sh script must then be run to generate the required test data. This will create the Resources directory heirarchy that stores
the random binary data files.

A port and ip that the server will be available on must be added to the config.py of each server directory. Each server directory has its own config.py file 
to add required configuration information for that server. 

### Run Instructions ###
Each server must then be run in its own terminal by navigating to its root directory e.g for the KeyServer server it would be 'cd KeyServer'. Then the server may be run
using uvicorn by
    'uvicorn main:app --port {}' and the port to run on.