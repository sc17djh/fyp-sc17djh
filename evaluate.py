from concurrent import futures
from concurrent.futures import ThreadPoolExecutor, as_completed
import random
import time
import sys
import os

from Client.client import get_patient_record, upload_record, get_patient_record, encrypt_record, get_dek
from Client.user import User

class Evaluate():

    def __init__(self):   
        self.file_dir = os.path.dirname(os.path.abspath(__file__))
        
        # create test users
        self.user_doctor = User('doctor1', 'password1', 'doctor')
        self.user_pharmacist = User('pharmacist1', 'password1', 'pharmacist')
        self.user_researcher = User('researcher1', 'password1', 'researcher')
        self.user_patient = User('patient1', 'password1', 'patient')
        self.user_paramedic = User('paramedic1', 'password1', 'paramedic')


    def evaluate_user_scalability_download(self, patient_id, record_id, concurrent_users):
        '''
        Make concurrent_users concurrent requests to the server to request a record
        '''
        # for each thread make a request to the API
        start_time = None
        with ThreadPoolExecutor(max_workers = concurrent_users) as executor:
            start_time = time.time()

            for _ in range(concurrent_users):
                executor.submit(get_patient_record, self.user_doctor, patient_id, record_id, decrypt=False)

        end_time = time.time()
        elapsed_time = end_time - start_time

        # print report table
        print(elapsed_time)

    
    def evaluate_user_scalability_upload(self, size, concurrent_users):
        '''
        Make concurrent_users concurrent requests to the server to upload a record
        '''
        # for each thread make a request to the API
        file_name = os.path.join(self.file_dir, 'Resources/Evaluation/Bytes',  'bytes{}MB'.format(size))
        key = encrypt_record(file_name)[0]

        start_time = None
        with ThreadPoolExecutor(max_workers = concurrent_users) as executor:
            start_time = time.time()

            for _ in range(concurrent_users):
                patient_id = random.randint(1000, 1000000000)
                executor.submit(upload_record, self.user_doctor, patient_id, "general", "test upload", file_name, key)

        end_time = time.time()
        elapsed_time = end_time - start_time

        # print report table
        print(elapsed_time)


    def evaluate_decrypt(self, patient_id, record_id, size):
        '''
        Fetch keys and encrypted object for each file size and decrypt, output runtime to compare decryption times
        '''
        start_time = time.time()

        get_patient_record(self.user_doctor, patient_id, record_id)

        end_time = time.time()
        elapsed_time = end_time - start_time

        # print report table
        print(elapsed_time)


    def evaluate_encrypt(self, patient_id, size):
        '''
        Generate keys and encrypt for each file size, produce a table to compare encryption times
        '''
        start_time = time.time()

        record_id = upload_record_transaction(self.user_doctor, 
                                        patient_id, 
                                        "general", 
                                        "encrypt and upload random binary file {}MB".format(size), 
                                        os.path.join(self.file_dir, 'Resources/Evaluation/Bytes',  'bytes{}MB'.format(size))
                                    )

        end_time = time.time()
        elapsed_time = end_time - start_time

        # print report table
        print(elapsed_time)
        return(record_id)


    def evaluate_dek_acquisition_time(self, patient_id, record_ids, concurrent_users):
        '''
        Make concurrent_users concurrent requests to the server to request a dek

        Shows scalability of the system with varying user loads to gain access to the data
        '''
        # for each thread make a request to the API
        total_time = 0
        with ThreadPoolExecutor(max_workers = concurrent_users) as executor:
            indexes = []
            for _ in range(concurrent_users):
                random_index = record_ids[random.randint(0, len(record_ids) - 1)]
                indexes.append(random_index)

            client_futures = (executor.submit(get_dek, self.user_doctor, patient_id, random_index) for random_index in indexes)

            for future in as_completed(client_futures):
                total_time += future.result()

        average_time = total_time / concurrent_users

        # print("users {} total_time {}".format(concurrent_users, total_time))
        return(average_time)


    def setup(self, sizes = ['100KB', '500KB', '1MB', '3MB', '5MB', '10MB', '25MB', '50MB', '100MB', '250MB'], quantity = [1,1,1,1,1,1,1,1,1,1]):
        '''
        Populate the server with required files for testing
        '''
        record_ids = {}
        record_ids.update({'all': []})
        for size in sizes:
            record_ids.update({str(size): []})
        patient_id = 'P001'

        print("Uploading test files for evaluation, sizes {}".format(sizes))
        for index in range(len(sizes)):
            for x in range(quantity[index]):
                record_id = upload_record(
                                self.user_doctor, 
                                patient_id, 
                                "general", 
                                "Random test binary file of size {} - test setup {}".format(sizes[index], x), 
                                os.path.join(self.file_dir, 'Resources/Evaluation/Bytes', 'bytes{}'.format(sizes[index]))
                            )
                record_ids[str(sizes[index])].append(record_id)
                record_ids['all'].append(record_id)
                

        return(record_ids)


def main():
    action = None
    actions = ['dek', 'object-size']

    print(sys.argv)
    if(len(sys.argv) == 2 and sys.argv[1] in actions):
        action = sys.argv[1]
    else:
        print("No action supplied. Please see below for usage,")
        print("USAGE:\n 'python3 evaluate.py {action}'")
        print("Possible actions are {}".format(actions))
        sys.exit(-1)

    # run the evaluation functions for the given parameters to automate the evaluation process
    eval = Evaluate()

    if(action == 'data-inflation'):
        sizes = ['100KB', '500KB', '1MB', '3MB', '5MB', '10MB', '25MB', '50MB', '100MB', '250MB']


        for i in range(len(users)):
            print("\n\n\n {} users".format(users[i]))
            print("\ndownload\n")
            # evaluate concurrent user download scalability
            for index in range(len(sizes)):
                eval.evaluate_user_scalability_download(1, index, users[i])

            print("\nupload\n")
            # evaluate concurrent user upload scalability
            for index in range(len(sizes)):
                eval.evaluate_user_scalability_upload(sizes[index], users[i])
    elif(action == "dek"):
        '''
        Show the time for an authorized user to obtain the DEK for a record with increasing number of concurrent user requests

        This is synonymous with the time to gain access to a resource.
        '''
        sizes = ['100KB', '500KB', '1MB', '3MB']
        quantity = [3,3,3,3]
        record_ids = eval.setup(sizes=sizes, quantity=quantity)
        concurrent_users = [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80]
        patient_id = 'P001'
        repeats = 20
    
        print("\n")
        print("\ndek acquisition time\n")
        # evaluate object-size download scalability
        for users in concurrent_users:
            total = 0.0
            for i in range(0, repeats):
                total += eval.evaluate_dek_acquisition_time(patient_id, record_ids['all'], users)
            average = total/repeats
            print(average)
    elif(action == "object-size"):
        '''
        Show the computation time for each entity in the design for varying DO sizes for uploading and downloading records
        '''
        sizes = ['100KB', '500KB', '1MB', '3MB', '5MB', '10MB', '25MB', '50MB', '100MB', '250MB']
        record_ids = []
        patient_id = 'P002'

        print("\n")
        print("\nupload\n")
        print("{} {} {} {} {}".format("total_time", "client_time", "server_time", "ta_time", "transfer_time"))
        # evaluate object-size upload scalability
        for index in range(len(sizes)):
            record_ids.append(      upload_record(eval.user_doctor, 
                                        patient_id, 
                                        "general", 
                                        "Random test binary file of size {}".format(sizes[index]), 
                                        os.path.join(eval.file_dir, 'Resources/Evaluation/Bytes', 'bytes{}'.format(sizes[index])),
                                        print_times = True
                                    )
            )
    
        print("\n")
        print("\ndownload\n")
        print("{} {} {} {} {}".format("total_time", "client_time", "server_time", "ta_time", "transfer_time"))
        # evaluate object-size download scalability
        for record_id in record_ids:
            get_patient_record(eval.user_doctor, patient_id, record_id, decrypt=True, print_times=True)
            

if __name__=="__main__":
    main()