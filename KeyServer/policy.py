# access control for key server based on permissions between roles and objects. Users are assigned to roles which in turn have permissions.

# create registry
import rbac.acl
acl = rbac.acl.Registry()

# register roles
acl.add_role("doctor")
acl.add_role("paramedic")
acl.add_role("pharmacist")
acl.add_role("researcher")
acl.add_role("patient")

# register resources
acl.add_resource('anonymised')
acl.add_resource('general')
acl.add_resource('medication')
acl.add_resource('private')
acl.add_resource('very private')

# create policy
acl.allow("researcher", "access", "anonymised")

acl.allow("pharmacist", "access", "medication")

acl.allow("doctor", "access", "general")
acl.allow("doctor", "write", "general")
acl.allow("doctor", "delete", "general")
acl.allow("doctor", "access", "private")
acl.allow("doctor", "write", "private")
acl.allow("doctor", "delete", "private")
acl.allow("doctor", "access", "very private")
acl.allow("doctor", "write", "very private")
acl.allow("doctor", "delete", "very private")
acl.allow("doctor", "access", "medication")
acl.allow("doctor", "write", "medication")
acl.allow("doctor", "delete", "medication")

acl.allow("paramedic", "access", "general")
acl.allow("paramedic", "access", "medication")
acl.allow("paramedic", "access", "private")

acl.allow("patient", "access", "general")
acl.allow("patient", "write", "general")
acl.allow("patient", "delete", "general")
acl.allow("patient", "access", "private")
acl.allow("patient", "write", "private")
acl.allow("patient", "delete", "private")
acl.allow("patient", "access", "very private")
acl.allow("patient", "write", "very private")
acl.allow("patient", "delete", "very private")
acl.allow("patient", "access", "medication")
acl.allow("patient", "access", "anonymised")
acl.allow("patient", "write", "anonymised")
acl.allow("patient", "delete", "anonymised")

