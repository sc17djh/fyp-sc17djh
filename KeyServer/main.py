from fastapi import FastAPI, Header, HTTPException, File, UploadFile, Form, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
import hashlib
import secrets
import time
import json
import os

import dbHooks
import models 
import policy

app = FastAPI()
db = dbHooks.Database("keys.db")

security = HTTPBasic()

# test endpoint
@app.get("/keys", status_code=200)
async def list_keys(patient: models.Patient = None):
    key_list = None

    try:
        if (patient and patient.patient_id):
            key_list = db.get_patient_key_list(patient.patient_id)
        else:
            key_list = db.get_key_list()
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="error on retrieving key list")

    # convert key list to JSON format
    json_keys = []
    try:
        if(key_list != None):
            json_keys = [{"key_id":key[0], "patient_id": key[1], "record_id": key[2], "key": key[3], "record_type": key[4]} for key in key_list]
    except KeyError as k_err:
        print("ERROR: Failed to parse key,\n{}".format(k_err))
        raise HTTPException(status_code=500, detail="Error on retrieving key list")
    json_key_list = {"keys": json_keys}
    return(json_key_list)


@app.get("/key", status_code=200)
async def get_key(record_pair: models.Record_Pair, credentials: HTTPBasicCredentials = Depends(security)):
    start_time = time.time()
    user_id, role = authenticate_user(credentials)

    key = None

    try:
        key = db.get_patient_key(record_pair.patient_id, record_pair.record_id)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="error on retrieving key")

    if(key == None):
        raise HTTPException(status_code=404, detail="key not found")

    # check user permissions
    if not(policy.acl.is_allowed(role, "access", key[1])):
        raise HTTPException(status_code=403, detail="Permission Denied")

    if (role == 'patient' and not user_id == record_pair.patient_id):
        raise HTTPException(status_code=403, detail="Permission Denied")
    
    computation_time = time.time() - start_time
    json_key = {'key': key[0], 'time': computation_time}
    return(json_key)


@app.post("/key", status_code=201)
async def save_key(key: models.Key, credentials: HTTPBasicCredentials = Depends(security)):
    user_id, role = authenticate_user(credentials)
    start_time = time.time()
    # check user permissions
    if not(policy.acl.is_allowed(role, "write", key.record_type)):
        raise HTTPException(status_code=403, detail="Permission Denied")

    if (role == 'patient' and not user_id == key.patient_id):
        raise HTTPException(status_code=403, detail="Permission Denied")

    try:
        db.create_key(key.patient_id, key.record_id, key.key, key.record_type)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="failed to save key")

    computation_time = time.time() - start_time
    return {"message": "key created", "time": computation_time}


@app.delete("/key", status_code=200)
async def delete_key(record_pair: models.Record_Pair, credentials: HTTPBasicCredentials = Depends(security)):
    user_id, role = authenticate_user(credentials)

    try:
        key = db.get_patient_key(record_pair.patient_id, record_pair.record_id)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="error on retrieving key")

    if(key == None):
        raise HTTPException(status_code=404, detail="key not found")

    # check user permissions
    if not(policy.acl.is_allowed(role, "delete", key[1])):
        raise HTTPException(status_code=403, detail="Permission Denied")

    if (role == 'patient' and not user_id == record_pair.patient_id):
        raise HTTPException(status_code=403, detail="Permission Denied")

    # delete key
    try:
        db.delete_key(record_pair.patient_id, record_pair.record_id)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="Failed to delete key")

    return({'message': 'deleted'})


def authenticate_user(credentials: HTTPBasicCredentials):
    '''
    Authenticate the user using HTTP Basic Auth
    '''
    error = HTTPException(
            status_code = 401,
            detail = "Incorrect username or password"
        )

    # check user credentials
    if(credentials is None):
        raise error

    user = db.get_user(credentials.username)
    password_correct = False

    if(user):
        # check password matches
        correct_password = user[2]
        password_correct = secrets.compare_digest(correct_password, credentials.password)

    if not(user and password_correct):
        raise error

    # return user id and role
    return((user[0], user[3]))


def revoke_user_privileges(user_id, new_role = None):
    '''
    Revoke a users privileges completely or use a new role
    '''
    start_time = time.time()

    if(new_role == "None"):
        db.update_user_role(user_id, new_role)
    else:
        db.delete_user(user_id)

    end_time = time.time()
    return(end_time - start_time)


def evaluate_revocation_time(revoked_user_num):
    '''
    Evaluate how revocation of user privileges scales with number of system users
    '''
    credentials = []
    for x in range(revoked_user_num):
        credentials.append(['U01{}'.format(x), 'doctor1{}'.format(x), 'password1', 'doctor'])

    db.populate_users_table(credentials)

    times = []
    for i in range(revoked_user_num):
        if(i == 1 or i % 5 == 0):
            times.append(revoke_user_privileges('U01{}'.format(i)))
        else:
            revoke_user_privileges('U01{}'.format(i))

    for time in times:
        print(time)


def grant_user_privileges(user_id, privilege_level):
    '''
    Measure the time to grant a user with a certain privilege level
    '''
    credentials = None
    if(privilege_level == "high"):
        credentials = [user_id, "doctorA{}".format(user_id), "password1", "doctor"]
    elif(privilege_level == "medium"):
        credentials = [user_id, "pharmacistA{}".format(user_id), "password1", "pharmacist"]
    elif(privilege_level == "low"):
        credentials = [user_id, "researcherA{}".format(user_id), "password1", "researcher"]

    start_time = time.time()
    db.create_user(*credentials)
    end_time = time.time()

    return(end_time - start_time)


def evaluate_grant_time(repeats = 5):
    '''
    Evaluate the average time to grant a user each privilege level
    '''
    # times shows accumulated times from low to high privilege
    times = [0.0, 0.0, 0.0]
    for i in range(repeats):
        times[0] += grant_user_privileges("Ug1{}".format(i), "low")
        times[1] += grant_user_privileges("Ug2{}".format(i), "medium")
        times[2] += grant_user_privileges("Ug3{}".format(i), "high")

    average_times = [time / repeats for time in times]
    for time in average_times:
        print(time)

if __name__=="__main__":
    # evaluate_revocation_time(100)

    evaluate_grant_time(repeats = 5)
    