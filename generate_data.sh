#!/bin/bash

echo Generating test binary files

# bash expr only accept integer inputs, so all MB file sizes must be put into KB
file_sizes=(
    100
    500
    1000
    3000
    5000
    10000
    25000
    50000
    100000
    250000
    500000
)

for size in "${file_sizes[@]}"; do
    size_in_MB=$((size / 1000))
    if (("$size_in_MB" > "0")); then
        echo Generating binary file of size "$size_in_MB" MB
        dd if=/dev/urandom of=bytes${size_in_MB}MB bs=1000 count=$size
    else
        echo Generating binary file of size "$size" KB
        dd if=/dev/urandom of=bytes${size}KB bs=1000 count=$size
    fi
done
