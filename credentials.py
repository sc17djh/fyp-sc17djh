# test credentials for use in evaluate.py

credentials = [
    ['doctor1', 'password1', 'doctor'],
    ['pharmacist1', 'password1', 'pharmacist'],
    ['researcher1', 'password1', 'researcher'],
    ['paramedic1', 'password1', 'paramedic'],
    ['patient1', 'password1', 'patient']
]