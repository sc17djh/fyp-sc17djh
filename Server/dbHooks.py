import pymysql
import sys
from datetime import datetime

class DatabaseError(Exception):
  pass 

class Database():

  def __init__(self, database_name):
    '''
    Initialise the database connection and cursor
    '''
    try:
      # setup connection
      self._db_connection = pymysql.connect("localhost", "root", "password", database_name)
      cursor = self._db_connection.cursor()
      self.initialise()

      # print version
      print('Connecting to MySQL database...')
      cursor.execute('SELECT VERSION()')
      db_version = cursor.fetchone()
      cursor.close()

    except Exception as err:
        print('Error: connection could not be established {}'.format(err))
        sys.exit(-1)
    else:
      print('Connection established\n{}'.format(db_version[0]))


  def initialise(self):
    '''
    Setup the database tables
    '''
    self.create_patient_record_table()
    self.create_users_table()

    credentials = [
        ['U001', 'doctor1', 'password1', 'provider'],
        ['U002', 'pharmacist1', 'password1', 'provider'],
        ['U003', 'researcher1', 'password1', 'provider'],
        ['U004', 'paramedic1', 'password1', 'provider'],
        ['P001', 'patient1', 'password1', 'patient'],
        ['P002', 'patient2', 'password2', 'patient']
    ]

    self.populate_users_table(credentials)


  def __del__(self):
    self._db_connection.close()


  def _query(self, cursor, query, params=None):
    try:
      self._log("query '{}', params '{}'\n".format(query, params))
      if(params):
        cursor.execute(query, params)
      else:
        cursor.execute(query)
    except Exception as err:
      self._log("ERROR: Exception on query '{}', params: '{}',\nerror: '{}'\n".format(query, params, err))
      # re-raise error to be caught further up incase of rollback requirements
      raise DatabaseError(err)


  def _log(self, message):
    print("{} : DB-LOG : {}".format(datetime.now(), message))


####################
### TEST METHODS ###
####################

  def create_patient_record_table(self):
    '''
    Create the patient record table.
    '''
    SQL = "CREATE TABLE records (\
            record_id    INT PRIMARY KEY AUTO_INCREMENT,\
            patient_id   VARCHAR(255) NOT NULL,\
            type         ENUM('anonymised', 'general', 'private', 'very private', 'medication') NOT NULL,\
            description  VARCHAR(100),\
            url          VARCHAR(255) NOT NULL UNIQUE\
          )"
    # try deleting the table first to prevent errors
    self.delete_patient_record_table()

    try:
      cursor = self._db_connection.cursor()
      self._query(cursor, SQL)
      self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)
    finally:
      cursor.close()
    

  def delete_patient_record_table(self):
    '''
    Delete the patient record table.
    '''
    SQL = "DROP TABLE IF EXISTS records"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL)
        self._db_connection.commit() 
    except DatabaseError as err:
      # rollback to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back DB")
      raise(DatabaseError(err))
    finally:
      cursor.close()


  def clear_patient_data(self):
    SQL = "DELETE FROM records"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL)
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
      self._log("rolled back db")
      raise DatabaseError(err)
    finally:
      cursor.close()


  def create_users_table(self):
    '''
    Create the users table.
    '''
    SQL = "CREATE TABLE users (\
            user_id           VARCHAR(20) PRIMARY KEY NOT NULL,\
            username          VARCHAR(20) UNIQUE NOT NULL,\
            password          VARCHAR(20) NOT NULL,\
            role              ENUM('patient', 'provider') NOT NULL\
          )"
    # try deleting the table first to prevent errors
    self.delete_users_table()

    try:
      cursor = self._db_connection.cursor()
      self._query(cursor, SQL)
      self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)


  def delete_users_table(self):
    '''
    Delete the users table.
    '''
    SQL = "DROP TABLE IF EXISTS users"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL)
        self._db_connection.commit() 
    except DatabaseError as err:
      # rollback to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back DB")
      raise(DatabaseError(err))


  def populate_users_table(self, credentials_list):
    for credentials in credentials_list:
      try:
        self.create_user(*credentials)
      except Exception as err:
        print("Failed to create user in test population,\n{}".format(err))
    print("Populated test users table")


####################
### USER METHODS ###
####################

  def get_record_url(self, patient_id, record_id):
    SQL = "SELECT url FROM records WHERE patient_id=%s AND record_id=%s"

    try:
      cursor = self._db_connection.cursor()
      self._query(cursor, SQL, (patient_id, record_id))
      results = cursor.fetchone()
      if(results == None):
        return(None)
      else:
        return(results[0])
    except Exception as err:
      raise DatabaseError(err)


  def create_record(self, patient_id, record_type, desc, url):
    SQL = "INSERT INTO records (patient_id, type, description, url)\
          VALUES (%s, %s, %s, %s)"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL, (patient_id, record_type, desc, url))
        results = cursor.lastrowid
        self._db_connection.commit()

        return(results)
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)
    finally:
      cursor.close()

      
  def get_user_record_list(self, patient_id):
    SQL = "SELECT record_id, type, description FROM records WHERE patient_id=%s"

    try:
      cursor = self._db_connection.cursor()
      self._query(cursor, SQL, (patient_id))
      results = cursor.fetchall()
      if(len(results) == 0):
        return(None)
      else:
        return(results)
    except Exception as err:
      raise DatabaseError(err)
    finally:
      cursor.close()


  def create_user(self, user_id,  username, password, role):
    '''
    Create a user account

    users may be patients or providers
    '''
    SQL = "INSERT INTO users (user_id, username, password, role)\
          VALUES (%s, %s, %s, %s)"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL, (user_id, username, password, role))
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)


  def delete_user(self, username):
    SQL = "DELETE FROM users WHERE username=%s"

    try:
        cursor = self._db_connection.cursor()
        self._query(cursor, SQL, (username))
        self._db_connection.commit()
    except Exception as err:
      # revert in case there are any errors to maintain consistency
      if(self._db_connection):
        self._db_connection.rollback()
        self._log("rolled back db")
      raise DatabaseError(err)


  def get_user(self, username):
    SQL = "SELECT * FROM users WHERE username=%s"

    try:
      cursor = self._db_connection.cursor()
      self._query(cursor, SQL, (username,))
      results = cursor.fetchone()
      if(results):
        return(results)
      else:
        return(None)
    except Exception as err:
      raise DatabaseError(err)

  
  def get_user_list(self):
    SQL = "SELECT * FROM users"

    try:
      cursor = self._db_connection.cursor()      
      self._query(cursor, SQL)
      results = cursor.fetchall()
      if(len(results) == 0):
        return(None)
      else:
        return(results)
    except Exception as err:
      raise DatabaseError(err)
