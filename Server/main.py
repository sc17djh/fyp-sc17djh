from fastapi import FastAPI, Header, HTTPException, File, UploadFile, Form, Depends
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from starlette.responses import FileResponse
import hashlib
import secrets
import json
import time
import os

import dbHooks
import models
import fs 

app = FastAPI()
db = dbHooks.Database("patient_data")

security = HTTPBasic()

record_fs_path = "Files/"

# test endpoint
@app.get("/list", status_code=200)
async def list_records(patient: models.Patient):
    record_list = None

    try:
        record_list = db.get_user_record_list(patient.patient_id)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="Could not retrieve patient record list")

    if (record_list == None):
        raise HTTPException(status_code=404, detail="No records exist for supplied patient ID")

    # convert record list to JSON format
    json_record_list = {"records": [{"record_id": record[0], "record_type": record[1], "desc": record[2]} for record in record_list]}
    return(json_record_list)
    

@app.get("/record", status_code=200)
async def get_record(record_pair: models.Record_Pair, credentials: HTTPBasicCredentials = Depends(security)):
    start_time = time.time()

    # authorize user
    id, role = authenticate_user(credentials)

    if not(role == 'provider' or id == record_pair.patient_id):
        raise HTTPException(status_code=403, detail="Permission Denied")

    try:
        record_url = db.get_record_url(record_pair.patient_id, record_pair.record_id)
    except Exception as err:
        print("Database Error: {}".format(err))
        raise HTTPException(status_code=500, detail="Could not retrieve URL of patient record")

    if(record_url is None):
        raise HTTPException(status_code=404, detail="Patient record not found")
    
    computation_time = time.time() - start_time
    try:
        return(FileResponse(os.path.join("Files", record_url), headers={'time': str(computation_time)}))
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="Could not retrieve patient record")


@app.post("/record", status_code=201)
async def post_record(patient_id: str = Form(...), record_type: str = Form(...), desc: str = Form(...), 
                        file: UploadFile = File(...), credentials: HTTPBasicCredentials = Depends(security)):
    time1 = time.time()
    # authorize user
    id, role = authenticate_user(credentials)

    if not(role == 'provider' or id == patient_id):
        raise HTTPException(status_code=403, detail="Permission Denied")

    # generate url to be unique for each unique file
    m = hashlib.md5()
    m.update(patient_id.encode('utf-8'))
    m.update(record_type.encode('utf-8'))
    m.update(desc.encode('utf-8'))

    filename = "{}-{}".format(patient_id, m.hexdigest())
    url = os.path.join(record_fs_path, filename)

    # check file doesn't already exist
    if (os.path.exists(url)):
        # if it exists then user should update existing file using PUT
        print("File already exists\n")
        raise HTTPException(status_code=409, detail="Item already exists")
        
    record_id = None
    try:
        # save file in filesystem
        with open(url, 'wb') as fp:
            for chunk in iter(lambda: file.file.read(10000), b''):
                fp.write(chunk)
        
        record_id = db.create_record(patient_id, record_type, desc, filename)
    except Exception as err:
        print(err)
        raise HTTPException(status_code=500, detail="Could not create record")
    computation_time = time.time() - time1
    return {"message": "created", "record_id": record_id, "time": computation_time}


def authenticate_user(credentials: HTTPBasicCredentials):
    error = HTTPException(
            status_code = 401,
            detail = "Incorrect username or password"
        )
    print(credentials)
    # check user credentials
    if(credentials is None):
        raise error

    user = db.get_user(credentials.username)
    password_correct = False

    print(user)
    if(user):
        print("user")
        # check password matches
        correct_password = user[2]
        print(correct_password)
        print(credentials.password)
        password_correct = secrets.compare_digest(correct_password, credentials.password)

    print(password_correct)
    if not(user and password_correct):
        raise error

    # return user id and role
    return((user[0],user[3]))