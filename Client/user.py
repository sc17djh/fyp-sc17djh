import base64

class User():

    def __init__(self, username, password, role):
        self.username = username
        self.password = password
        self.auth = self.create_authorization_basic_header()
        self.role = role

    
    def create_authorization_basic_header(self):
        credentials_b64 = base64.b64encode("{}:{}".format(self.username, self.password).encode('utf-8'))
        header = {'Authorization': "Basic {}".format(credentials_b64.decode('utf-8'))}

        return(header)